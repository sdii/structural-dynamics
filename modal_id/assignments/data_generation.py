# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

# Importing some python packages
import numpy as np
import matplotlib.pyplot as plt
#%matplotlib inline
#from IPython.display import display, Math

# There is no need to pay much attention to this block of text.  This is setting up the code for easy visualization of results
np.set_printoptions(formatter={'float':lambda x: '{:^ 9.0f}'.format(0) if x==0 else '{:^ 9.2e}'.format(x)}, 
                    suppress=True, linewidth=500)

# Outside and inside diameters
do = 6.0  # in
di = 6.0 - 2.0/8.0  # in

# Calculating the area of the cross section
A = np.pi * (do/2.0) ** 2 - np.pi * (di/2.0) ** 2

# Calculating the moment of inertia
I = np.pi * (do**4 - di**4) / 64.0

E = 1e7                        # Young modulus in psi
L1 = L3 = L5 = 10.0*12.0       # Changing units to inches
L2 = L4 = 11.0*12.0 + 8.0      # Length in inches

#%% Stiffness matrix

def localk(A,E,I,L):
    """
    This function calculates the stiffness matrix in local coordinates
    
    Args:
        A (float): Cross sectional area of the beam
        E (float): Young modulus of the material
        I (float): Moment of inertia of the cross sectional area
        L (float): Length of the beam
        
    Returns:
        ke (numpy.matrix): Matrix of the structural member in local coordiantes
    """
    ke = np.matrix([[A*E/L, 0, 0, -A*E/L, 0, 0],
          [0, 12*E*I/L**3, 6*E*I/L**2, 0, -12*E*I/L**3, 6*E*I/L**2],
          [0, 6*E*I/L**2, 4*E*I/L, 0, -6*E*I/L**2, 2*E*I/L],
          [-A*E/L, 0, 0, A*E/L, 0, 0],
          [0, -12*E*I/L**3, -6*E*I/L**2, 0, 12*E*I/L**3, -6*E*I/L**2],
          [0, 6*E*I/L**2, 2*E*I/L, 0, -6*E*I/L**2, 4*E*I/L]])
    return ke

# Forming transformation matrix
def Tmatrix(phi):
    """
    This function defines the transformation matrix
    
    Args:
        phi (float): Angle between local and global coordiantes (2d)
        
    Returns:
        T (numpy.matrix): Transformation matrix
    """
    T = np.matrix([[np.cos(phi), np.sin(phi), 0, 0, 0, 0],
         [-np.sin(phi), np.cos(phi), 0, 0, 0, 0],
         [0, 0, 1.0, 0, 0, 0],
         [0, 0, 0, np.cos(phi), np.sin(phi), 0],
         [0, 0, 0, -np.sin(phi), np.cos(phi), 0],
         [0, 0, 0, 0, 0, 1.0]])
    return T

# Calculating the stiffness matrix for the first and second element (local coordinates)
k1 = localk(A,E,I,L1)
k2 = localk(A,E,I,L2)

# Forming elements in global coordinates
T1 = Tmatrix(-np.pi/2)
T5 = Tmatrix(np.pi/2)
k2_global = k4_global = k2
k1_global = k3_global = np.dot(np.dot(T1.transpose(),k1),T1)  # In python we use np.dot for matrix multiplication
k5_global = np.dot(np.dot(T5.transpose(),k1),T5)

# Matrix assembly
def assembly(K,k_element,DOF):
    """
    This function helps us construct the stiffness matrix of the structure
    
    Args:
        K (numpy.matrix): Stiffness matrix of the structure
        k_element (numpy.matrix): Stiffness matrix of the element
        DOF (list): Degrees of freedom of the element
    """
    counter = [0, 1, 2, 3, 4, 5]
    for row, dof_row in zip (counter,DOF):
        for col, dof_col in zip(counter,DOF):
            K[dof_row,dof_col] = K[dof_row,dof_col] + k_element[row,col]


# Matrix assembly
K = np.zeros((18,18))
assembly(K,k1_global,[0,1,2,3,4,5])
assembly(K,k2_global,[3,4,5,6,7,8])
assembly(K,k3_global,[6,7,8,9,10,11])
assembly(K,k4_global,[6,7,8,12,13,14])
assembly(K,k5_global,[12,13,14,15,16,17])

# Applying boundary conditions
K = np.delete(K,[9,10,11],0)  # The zero at the end indicates that  rows should be deleted
K = np.delete(K,[9,10,11],1)  # The one ad the end indicates that columns should be deleted

#%% Mass matrix
rho = 3.043e-3        # Density of aluminum in slugs/in^3

# Calculating the mass for each element
m1 = m3 = m5 = A*L1*rho
m2 = m4 = A*L2*rho

def localm(m):
    """
    This function calculates the mass matrix in local coordinates (lumped mass model)
    
    Args:
        m (float): Total mass of the element
        
    Returns:
        me (numpy.matrix): Matrix of the structural member in local coordiantes
    """
    me = np.matrix([[m/2, 0, 0, 0, 0, 0],
          [0, m/2, 0, 0, 0, 0],
          [0, 0, 0, 0, 0, 0],
          [0, 0, 0, m/2, 0, 0],
          [0, 0, 0, 0, m/2, 0],
          [0, 0, 0, 0, 0, 0]])
    return me

# Calculating the mass matrices
m1_local = m3_local = m5_local = localm(m1)
m2_local = m4_local = localm(m2)

# Matrix assembly
M = np.zeros((18,18))
assembly(M,m1_local,[0,1,2,3,4,5])
assembly(M,m2_local,[3,4,5,6,7,8])
assembly(M,m3_local,[6,7,8,9,10,11])
assembly(M,m4_local,[6,7,8,12,13,14])
assembly(M,m5_local,[12,13,14,15,16,17])

# Boundary conditions
M = np.delete(M,[9,10,11],0)  # The zero at the end indicates that  rows should be deleted
M = np.delete(M,[9,10,11],1)  # The one ad the end indicates that columns should be deleted

#%% Applying guyan reduction
# DOF to condense
odof = [2, 5, 8, 11, 14]
# DOF to keep
tdof = [0, 1, 3, 4, 6, 7, 9, 10, 12, 13]

mtt = M[np.ix_(tdof,tdof)]
ktt = K[np.ix_(tdof,tdof)]
kto = K[np.ix_(tdof,odof)]
kot = K[np.ix_(odof,tdof)]
koo = K[np.ix_(odof,odof)]

# Matrix to use in subsequent analysis (reduced)
K = ktt - np.dot(kto,np.dot(np.linalg.inv(koo),kot))
M = mtt;

#%% Modal parameters
# Calculating the inverse of the mass
invM = np.linalg.inv(M)

# Calculatingthe eigenvalues and eigenvectors
w2,Phi = np.linalg.eig(np.dot(invM,K))

# Sorting the results
idx = w2.argsort()   
w2 = w2[idx]
Phi = Phi[:,idx]

omega = np.sqrt(w2)
f = omega/2/np.pi

#%% Initial conditions
q = np.zeros((10,1))

#%% Matrix in modal coordiantes
Kn = np.dot(np.dot(Phi.transpose(),K),Phi)
Mn = np.dot(np.dot(Phi.transpose(),M),Phi)

#%% Defining Delta_t
dt = 1.0/2000
tfinal = 20.0

# Defining the time vector
tmp = range(int(tfinal/dt)+1)
t = [ti*dt for ti in tmp]
t = np.array(t)

#%% Defining the matrix P
P = np.zeros((len(K),len(t)))
sizeP = np.shape(P)

# Assigning 224lb to the 7th row for t < 1
# P[7,0:int(1/dt)] = -224   # Negative because it is going downward in the problem definition

# Assigning 112 lb to the 7th row for t > 1
# P[7,int(1/dt):] = -112  # Negative because it is going downward in the problem definition
P[7,0] = 5000

# Plotting the force for the 8th DOF (7th row)
plt.plot(t,P[7,:]);
plt.xlabel ('Time (s)');
plt.ylabel ('Force (lb)');

# Calculating Pn.  In python np.dot() is used for matrix multiplication
Pn = np.dot(Phi.transpose(),P)

# Defining a python function with the central difference method for a SDOF
def cdm(k,m,c,p,u_o,udot_o,dt):
    """
    Calculates the responce of a SDOF using the central difference method
    
    Args:
        k (float): stiffness of the SDOF
        m (float): mass of the SDOF
        c (float): damping of the SDOF
        p (numpy.array): vector of forces
        u_o (float): initial displacement
        udot_o (float): initial velocity
        dt (float): delta of time between steps
        
    Returns:
        u (numpy.array): displacement vector
    """
    
    # Initializing variables
    u = np.zeros(len(p))
    u[0] = u_o
    udot = np.zeros(len(p))
    udot[0] = udot_o
    uddot = np.zeros(len(p))
    uddot[0] 
    
    # Initial calculations
    uddot[0] = (p[0] - c*udot_o - k*u_o) / m
    u[-1] = u[0] - dt*udot[0] + (dt)**2 * uddot[0] / 2
    k_hat = m/(dt**2) + c/(2*dt)
    a = m/(dt**2) - c/(2*dt)
    b = k - 2*m/(dt**2)
    
    for i in range(len(p)-1):
        pi_hat = p[i] - a*u[i-1] - b*u[i]
        u[i+1] = pi_hat / k_hat
        udot[i] = (u[i+1] - u[i-1])/(2*dt)
        uddot[i] = (u[i+1] - 2*u[i] + u[i-1]) / (dt**2)
        
    return u


# Applying the central difference method for each q
q = np.zeros((len(omega),len(t)))
for n in range(len(omega)):
    # Calculate cn
    cn = 2*Mn[n,n]*omega[n]*0.05
    # Calculate qn using central difference method
    q[n,:] = cdm(Kn[n,n],Mn[n,n],c=cn, p=Pn[n,:] ,u_o=0, udot_o=0, dt = dt)
    
# Calculating {u}
u = np.dot(Phi,q)

#%% Resample
tout = t[range(0,len(t),6)]
uout = u[:,range(0,len(t),6)]

#%% Plotting the results
plt.clf()
for k in range(1,10,2):
    plt.plot (tout,uout[k,:])
plt.ylabel ('Displacement (m)')
plt.xlabel ('Time (s)')
plt.legend(['1','2','3','5','6'])

np.savetxt('data.txt',np.vstack([tout,uout[range(1,10,2),:]]).transpose(), delimiter = ' ', fmt = '%1.4e', newline='\n')