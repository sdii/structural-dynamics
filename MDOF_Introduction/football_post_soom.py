# -*- coding: utf-8 -*-
"""
Created on Tue Sep 02 08:32:46 2014

@author: caicedo
"""

import soom as soom
import numpy as np

L1 = 10*12
L2 = 11*12+8
bc2d = [False, False, True, True, True, False]

# Coordinates
coord = [[0,2*L1,0],
        [0,L1,0],
        [L2,L1,0],
        [L2,0,0],
        [2*L2,L1,0],
        [2*L2,2*L1,0]]

# Creating list of nodes
nlist = [soom.fem.Node(ele,bc=bc2d) for ele in coord]

# Applying boundary conditions to node 3
nlist[3].bc = [True]*6

# Cross section
do = 6.0  # in
di = 6.0 - 2.0/8.0  # in
A = np.pi * (do/2.0) ** 2 - np.pi * (di/2.0) ** 2
I = np.pi * (do**4 - di**4) / 64.0
s = soom.geometry.Section(name="Section", area = A, iy = I, iz = I)

# Material
mat = soom.materials.Isolinear(name="Aluminum", e = 1e7)

# Connectivity of elements
conn = [[0, 1],
        [1, 2],
        [2, 3],
        [2, 4],
        [4, 5]]

# Creating elements
ele = []
for c in conn:
    ele.append(soom.fem.Ebbeam([nlist[c[0]],nlist[c[1]]], section = s, material = mat))

[el.plot() for el in ele]

# Creating the structure
mymodel = soom.fem.FemModel(ele,name="Football goal")

# Numbering and calculating stiffness matrix
mymodel.numberNodes()
mymodel.plot()
K = mymodel.globalk()
F = np.matrix([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -750, 0, 0, 0, 0])
F = F.transpose()
Kinv = np.linalg.inv(K.todense())  
u = np.dot(Kinv,F)        
print (u)
